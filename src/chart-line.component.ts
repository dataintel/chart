import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

export const lineComponentOptions = {
  selector: 'chart-line',
  template: `<div class="row">
                <div class="col-md-6">
                  <div style="display: block; width: 500px; height: 300px;">
                  <canvas baseChart 
                              [datasets]="lineChartData"
                              [labels]="lineChartLabels"
                              [options]="lineChartOptions"
                              [colors]="lineChartColors"
                              [legend]="lineChartLegend"
                              [chartType]="lineChartType"
                              (chartHover)="chartHovered($event)"
                              (chartClick)="onClickLineChart($event)"></canvas>
                  </div>
                </div>
                <div>
                  <button md-raised-button *ngIf="lineChartMonth" (click)="backSentimentLineChartClicked()" color="accent">Buttons</button>
              </div>`,
  styles: [``]
};

@Component(lineComponentOptions)

export class ChartLineComponent implements OnInit {

  @Input() lineChartData: Array<any> = [{ data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A', fill: false }];
  @Input() lineChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  @Input() lineChartTooltips: Array<any>;
  @Input() lineChartColors: Array<any>;
  @Input() lineChartLegend: boolean = true;
  @Output() lineChartClickData: EventEmitter<any> = new EventEmitter();
  @Output() clickDataMonth: EventEmitter<any> = new EventEmitter();
  @Input() lineChartMonth = false;

  lineChartType: string = 'line';

  public index_line: number;

  public lineChartOptions = {
    animation: {
      duration: 10,
    },
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {},
    elements: {
      line: {
        borderWidth: 1
      },
      point: {
        radius: 5,
        hitRadius: 0,
        hoverRadius: 4,
      },
    },
    legend: { display: true, position: 'bottom' },
    scales: {
      xAxes: [{
        stacked: false,
        gridLines: { display: false },
        ticks: {
          autoSkip: true,
          maxRotation: 0
        }
      }],
      yAxes: [{
        stacked: false,
        ticks: {
          beginAtZero: false,
          callback: function (label: any, index: any, labels: any) {
            if (Math.floor(label) === label) {
              return (label >= 1000) ? label / 1000 + 'k' : label;
            }
          }
        }
      }],
    }, // scales
  }; // options

  constructor() { }

  ngOnInit() {

  }

  onClickLineChart(event: any) {
    if (typeof event.active[0] !== 'undefined' && event.active[0]._index !== 'undefined') {
      this.index_line = event.active[0]._index;
      this.lineChartMonth = true;
      this.lineChartClickData.emit(event);
    } else {
      this.index_line = null;
    }
  }

  backSentimentLineChartClicked() {
    this.lineChartMonth = false;
    this.lineChartClickData.emit(event);
  }

}


