import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

export const pieComponentOptions = {
    selector: 'chart-pie',
    template: `<div style="display: block; width: 500px; height: 200px;">
                <canvas baseChart 
                    class="chart" 
                    [data]="pieChartData" 
                    [labels]="pieChartLabels" 
                    [legend]="legend" 
                    [options]="pieChartOptions" 
                    [chartType]="pieChartType" 
                    [colors]="pieChartColors">
                </canvas>
            </div>`,
    style: [``]
};

@Component(pieComponentOptions)

export class ChartPieComponent implements OnInit {

    @Input() pieChartLabels: String[] = ['Positive', 'Netral', 'Negative'];
    @Input() pieChartData: Number[] = [300, 500, 100];
    @Input() pieChartType: String = 'pie';
    @Input() pieChartColors: any;
    @Input() pieChartPercentage: Boolean = true;
    @Input() legend = true;
    public pieChartOptions = {};

    chartClicked(e: any): void {
        console.log(e);
    }

    chartHovered(e: any): void {
        console.log(e);
    }
    constructor() { }

    ngOnInit() {
        if (this.pieChartPercentage) {
            this.pieChartOptions = {
                animation: {
                    duration: 10,
                },
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    position: 'right',
                    labels: {
                        boxWidth: 15,
                        padding: 5
                    }
                },
                tooltips: {
                    mode: 'label',
                    callbacks: {
                        label: function (tooltipItem: any, data: any) {
                            let dataset = data.datasets[tooltipItem.datasetIndex];
                            let total = dataset.data.reduce(
                                function (previousValue: any, currentValue: any, currentIndex: any, array: any) {
                                    return previousValue + currentValue;
                                });
                            let currentValue = dataset.data[tooltipItem.index];
                            let precentage = ((currentValue / total) * 100).toFixed(2);
                            return precentage + '%';
                        }
                    }
                },
            };
        } else {
            this.pieChartOptions = {
                animation: {
                    duration: 10,
                },
                responsive: true,
                maintainAspectRatio: false,
                tooltips: {
                    mode: 'label',
                    callbacks: {
                        label: function (tooltipItem: any, data: any) {
                            return data.labels[tooltipItem.index] + ': ' + data.datasets[0].data[tooltipItem.index];
                        }
                    }
                },
                legend: {
                    position: 'right',
                    labels: {
                        boxWidth: 15,
                        padding: 5
                    }
                }
            };
        }
    }
}