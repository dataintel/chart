import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';
import { MdButtonModule, MdCheckboxModule } from '@angular/material';
import { MaterialModule } from '@angular/material';
import { ChartLineComponent } from './src/chart-line.component';
import { ChartPieComponent } from './src/chart-pie.component';
import { ChartColumnComponent } from './src/chart-column.component';
import { ChartBarComponent } from './src/chart-bar.component';

export * from './src/chart-line.component';
export * from './src/chart-pie.component';
export * from './src/chart-column.component';
export * from './src/chart-bar.component';

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    MdButtonModule,
    MdCheckboxModule,
    MaterialModule
  ],
  declarations: [
    ChartLineComponent,
    ChartPieComponent,
    ChartColumnComponent,
    ChartBarComponent
  ],
  exports: [
    ChartLineComponent,
    ChartPieComponent,
    ChartColumnComponent,
    MdButtonModule,
    MdCheckboxModule,
    MaterialModule,
    ChartBarComponent
  ]
})
export class ChartModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ChartModule,
      providers: []
    };
  }
}
