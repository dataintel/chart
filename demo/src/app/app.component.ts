import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  public legend = false;

  public lineChartData: Array<any> = [
    {
      data: [65, 59, 80, 81, 56, 55, 40],
      label: 'Positive',
      fill: false
    },
    {
      data: [95, 89, 70, 61, 56, 45, 30],
      label: 'Netral',
      fill: false
    },
    {
      data: [15, 29, 30, 41, 56, 65, 70],
      label: 'Negative',
      fill: false
    }
  ];

  public lineChartLabels: Array<any> = ['01', '02', '03', '04', '05', '06', '07'];
  public  lineChartMonth = false;

  onDrilldown(event: any) {
    console.log(event);
    this.lineChartMonth = true;
    this.lineChartLabels = [
      '01', '02', '03', '04', '05', '06', '07', '08', '09', '10',
      '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
      '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'];

    // this.lineChartData = [{
    //   data: [
    //     15, 29, 30, 41, 56, 65, 70, 80, 90, 10,
    //     15, 29, 30, 41, 56, 65, 70, 80, 90, 10,
    //     15, 29, 30, 41, 56, 65, 70, 80, 90, 10, ],
    //   label: 'Negative',
    //   fill: false
    // }
    // ];

    // this.lineChartData = [{ data: [15, 29, 30, 41, 56, 65, 70, 80, 90, 10,
    //     15, 29, 30, 41, 56, 65, 70, 80, 90, 10,
    //     15, 29, 30, 41, 56, 65, 70, 80, 90, 10], label: 'Series A', fill: false }];
  }
}

